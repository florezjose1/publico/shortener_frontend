import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';
import Login from './scenes/Auth/Login/login'
import Shorten from "./scenes/shorten/shorten";
import Admin from "./scenes/Admin/admin";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Route exact path="/" component={Login}/>
                    <Route path="/shorten" component={Shorten}/>
                    <Route path="/admin" component={Admin}/>
                </div>
            </Router>
        );
    }
}

export default App;
