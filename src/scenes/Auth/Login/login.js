import {Component} from "react";
import React from "react";
import './login.css';
import Typography from "@material-ui/core/es/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import Logo from "./../../../components/Logo";

export default class Login extends Component {
    state = {
        name: 'admin@example.com',
        password: 'admin123',
    };

    render() {
        return (
            <div className="login">
                <div className="contentLogin">
                    <div className="labelCard loginLabel">
                        <Logo/>
                    </div>

                    <form action="/admin" className="formLogin" noValidate autoComplete="off">
                        <TextField
                            id="outlined-name"
                            label="Username"
                            className="form-input"
                            value={this.state.name}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Password"
                            type="password"
                            className="form-input"
                            value={this.state.password}
                            margin="normal"
                            variant="outlined"
                        />

                        <div className="actionForm">
                            <Button variant="outlined" className="" type="submit" color="primary">
                                Login
                            </Button>

                            <Typography variant="subtitle2" className="margin-top-15" gutterBottom>
                                Shorten link <Link to="/shorten">shorten</Link>
                            </Typography>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}