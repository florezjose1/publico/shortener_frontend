import React, {Component} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

let id = 0;

function createData(alias, outgoing_url, created) {
    id += 1;
    return {id, alias, outgoing_url, created};
}

const rows = [
    createData('wnkasdiwasd', 'joseflorez.co', '9 minutes'),
    createData('wnkasdiwasd', 'joseflorez.co', '5 minutes'),
    createData('wnkasdiwasd', 'joseflorez.co', '2 minutes'),
    createData('wnkasdiwasd', 'joseflorez.co', '20 minutes'),
];


export default class ListLinks extends Component {
    render() {
        return (
            <div>
                <Paper className="">
                    <Table className="">
                        <TableHead>
                            <TableRow>
                                <TableCell>ALIAS</TableCell>
                                <TableCell align="right">OUTGOING URL</TableCell>
                                <TableCell align="right">CREATED</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.alias}
                                    </TableCell>
                                    <TableCell align="right">{row.outgoing_url}</TableCell>
                                    <TableCell align="right">{row.created}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        )
    }
}