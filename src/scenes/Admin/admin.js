import {Component} from "react";
import React from "react";
import './admin.css'
import Logo from "./../../components/Logo";
import Typography from "@material-ui/core/Typography";
import TabAdmin from "./components/TabAdmin";

export default class Admin extends Component {

    render() {
        return (
            <div className="admin">
                <div className="contentAdmin">
                    <div className="labelCard adminLabel">
                        <Logo/>
                    </div>
                    <div className="contentDataAdmin">
                        <Typography variant="h3" gutterBottom className="">
                            Administrator
                        </Typography>
                        <div className="LinksAdmin">

                            <TabAdmin/>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
